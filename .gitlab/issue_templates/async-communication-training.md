<!--
Use issue title:
[YOUR NAME]: Async Communication Verify Team Training (FY##-Q#)
-->

### Overview :muscle: 

This training is to help refresh our entire stage across engineering, product, and design to better work together in our values and communication practices. Each quarter, the PMs/EMs [will vote](https://gitlab.com/gitlab-org/verify-stage/-/issues/50) from three topics from the [GitLab Competencies](https://about.gitlab.com/handbook/competencies/#list) for the stage to complete a processes refresh training. This quarter the topic selected was: **Embracing asynchronous communication**

### Training Tasks :lifter: 

- [ ] Watch [Bias toward Async training with Sid](https://youtu.be/_okcPC9YucA)
- [ ] Read [When to use Async and Sync Communication](https://about.gitlab.com/company/culture/all-remote/asynchronous/#when-to-use-asynchronous-and-synchronous-communication)
- [ ] Read [Reduce Reliance on Slack](https://about.gitlab.com/company/culture/all-remote/asynchronous/#reducing-reliance-on-slack-and-synchronicity)
- [ ] Read [Understanding Low context communication](https://about.gitlab.com/company/culture/all-remote/effective-communication/#understanding-low-context-communication)
- [ ] Attend Sync Meeting on [Issue Refinement](https://docs.google.com/presentation/d/1Y108s_jnvFAXCmLMiAZBIfqL7AQTkRgDdI5LUnzrMcY/edit?usp=sharing)
- [ ] Complete [Survey](https://forms.gle/zKUPLGizJxoV7NCZ9)
