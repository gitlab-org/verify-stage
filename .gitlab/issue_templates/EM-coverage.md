## Intent
This issue describes how broader group and team members are facilitating an Engineering Manager to take meaningful time away from work](https://about.gitlab.com/handbook/paid-time-off/). The intent is that the EM isn't required to "catchup" before and after taking a well-deserved vacation. 

## :handshake: Responsibilities
The following are a list of known responsibilities of an EM, but are not limited to the following:

- tracking progress of issues during a milestone, removing roadblocks as needed for the team
- running team sync meetings
- assigning issues to engineers for investigation (or weighting / refinement, if applicable)
- assigning community contribution MRs for review
- hiring or interviewing candidates for their teams
- triage reports: weekly bug triage, following up on idle MRs, overdue feature flag cleanup
- group retrospectives (if applicable)
- responding to any other pings on Slack or on issues


| Priority | Theme | Context Link | Primary | Secondary |
| -------- | ----- | ------------ | ------- | --------- |
| HIGH/MEDIUM/LOW | Respond to any customer questions | [Direction Page]() | Your Manager | Your Designer/EM | 

## :muscle: Coverage Tasks
Include specific issue links for tasks to be completed. Solution validation issues to progress, kickoff, PI or GC prep, etc.
- [ ] Item - Link - `@assignee`

## :book: References
Here are some references for how I and my group generally works:
- [Group Process Handbook Page](ADD LINK)
- [Additional References](ADD LINK)

## :white_check_mark: Issue Tasks

### :o: Opening Tasks
- [ ] Assign to yourself
- [ ] Title the issue `EM Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
- [ ] Add an issue comment for your :recycle: Retrospective Thread
- [ ] Add any relevant references including group handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your section, stage and group Slack channels
- [ ] Ensure you've assigned tasks via PTO Ninja including assigning some tasks to a relevant #g_ , #s_ or #product slack channel
- [ ] Ensure your PTO Ninja auto-responder points team members to this issue
- [ ] Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/#current-status) to include a reference to this issue

### :x: Closing Tasks
- [ ] Review the [Returning from Time Off ](https://about.gitlab.com/handbook/product/product-manager-role/#returning-from-time-off) guidance
- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective Items and update [this template](https://gitlab.com/gitlab-com/Product/-/blob/master/.gitlab/issue_templates/PM-Coverage.md)
