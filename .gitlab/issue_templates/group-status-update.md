## Announcements

<!-- Accomplishments, things to celebrate, tasks to-do (recurring or added) -->

## Milestone Tracking
<!-- For example: list issues or epics being tracked, and % of completion. Add overall health status for the milestone (e.g. committed vs planned) -->
- On Track | Needs Attention | At Risk 
  - x% of completion of Y
  - Managed [#incident-ABCDE]() and was not able to prioritize `fgh`

## Weekly Updates

### Incidents (if any)

<!-- List of incident(s) raised that were related to the team's domain -->

### Error Budget

<!-- Can be a copy/paste from the Slack update -->

### Issues related to Reliability 

<!-- You could add links to these queries so it's a matter of counting totals -->
- Infradev (Total: x, Past Due: y)
- Availability (Total: x, Past Due: y)
- Security (Total: x, Past Due: y)
- Corrective Action (Total: x, Past Due: y)

### OKRs

<!-- Link to Ally OKRs, provide an updated score -->

### Hiring 

<!-- List any open reqs and each of their status -->
