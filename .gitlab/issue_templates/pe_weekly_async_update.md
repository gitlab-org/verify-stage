## Announcements :mega:

- 

## Milestone Tracking :calendar:
<!-- For example: list issues or epics being tracked, and % of completion. Add overall health status for the milestone (e.g. committed vs planned) -->
- [{insert milestone} Scope of work](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/{Issue ID}#goals-for-the-milestone)
- [{insert milestone} Pipeline Execution board](https://gitlab.com/groups/gitlab-org/-/boards/1372896?label_name[]=group%3A%3Apipeline%20execution&milestone_title={insert milestone})

## Weekly Updates :pencil: 

### Incidents (if any)

None occurred this week.

----

### Error Budget :bar_chart: 

* Last week's 28d service [availability](https://dashboards.gitlab.net/d/stage-groups-detail-pipeline_execution/stage-groups-pipeline-execution-group-error-budget-detail?orgId=1) was {}%  
  * {} minutes remaining and {} minutes spent.
* The last 7d is [currently](https://dashboards.gitlab.net/d/stage-groups-detail-pipeline_execution/stage-groups-pipeline-execution-group-error-budget-detail?orgId=1&from=now-7d%2Fm&to=now%2Fm) at {}% 

----

### Issues related to Reliability :chart_with_upwards_trend: 

 * :construction: Infradev ([Total: {insert number}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20execution&label_name%5B%5D=infradev&first_page_size=20), [Past Due: {insert number}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20execution&label_name%5B%5D=infradev&label_name%5B%5D=missed-SLO&first_page_size=20))
 <!--    * # Past Due in ~"workflow::ready for development" -- {link to issues} is scheduled for milestone {milestone}. -->
 * :closed_lock_with_key: Security ([Total: {insert number}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20execution&label_name%5B%5D=security&label_name%5B%5D=bug%3A%3Avulnerability&first_page_size=20), [Past Due: {insert number}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20execution&label_name%5B%5D=security&label_name%5B%5D=bug%3A%3Avulnerability&label_name%5B%5D=missed-SLO&first_page_size=20))
 * :pencil2:  Corrective Actions ([Total: {insert number}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20execution&label_name%5B%5D=corrective%20action&first_page_size=20), [Past Due: {insert number}](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20execution&label_name%5B%5D=corrective%20action&label_name%5B%5D=missed-SLO&first_page_size=20))

----

### Issue Breakdown by Type - Pipeline Execution Core - Excluding CI SaaS Scaling

#### Milestone {insert milestone}

##### Deliverables in Milestone

| Ratio | ~type::feature | ~type::bug | ~type::maintenance | Calculation |
| ----- | -------------- | ---------- | ------------------ | ----------- |
| Target |  % | % | % | manually set in planning |
| Deliverable Issues |  % | % | % | (deliverable issues of type) / (total deliverable issues) |
| % of Closed Deliverable Issues |  % | % | % | (closed deliverable of type) / (total deliverables closed) |
| Deliverables Say/Do | % | % | % | (closed deliverable of type) / (deliverable issues of type) | 
| # Deliverable Issues |  |  |  | raw count |
| # Closed Deliverable Issues |  |  |  | raw count |

**Overall Deliverable Say/Do: {}%**

##### All Issues 
| Ratio | ~type::feature | ~type::bug | ~type::maintenance | Calculation |
| ----- | -------------- | ---------- | ------------------ | ----------- |
| Target |  % | % | % | manually set in planning |
| Issues in Milestone | % | % | % | (issues of type) / (total issues) |
| Closed Issues in Milestone |  % | % | % | (closed of type) / (total closed) |
| All Issues Say/Do | % | % | % | (closed of type) / (issues of type) |
| # Issues |  |  |  | raw count |
| # Closed Issues |  |  |  | raw count |

**Overall Say/Do: {}%**

##### MRs
| Ratio | ~type::feature | ~type::bug | ~type::maintenance | Calculation |
| ----- | -------------- | ---------- | ------------------ | ----------- |
| MRs in Milestone | % | % | % | (MRs of type) / (total MRs) |
| # MRs |  |  |  | raw count |

{image of issue breakdown}

*Note: Say/Do is calculated as closed of type / total (Deliverable) issues. It does not account for issues explicitly removed from the milestone.*

[Dashboard](https://app.periscopedata.com/app/gitlab/1068424/WIP---Verify-Milestone-Planning-Exploration)

----

### OKRs :bar_chart:

<!-- Link to Ally OKRs, provide an updated score -->
#### Engineering 

https://app.ally.io/teams/40387/objectives?tab=0&chartView=false&time_period_id=155987&selected_dashboard_id=0

{insert Ally graph for PE}

{List out in numbered order OKRs}

{}% complete for ~"group::pipeline execution" OKRs  
Updated Score - {}

#### Product 

https://app.ally.io/users/159655/objectives?tab=0&chartView=false&time_period_id=155987&selected_dashboard_id=0

{insert Ally graph for PE}

{List out in numbered order OKRs}

{}% complete for ~"group::pipeline execution" OKRs  
Updated Score - {}

----

### Hiring :ballot_box_with_check: 

<!-- List any open reqs and each of their status -->

## Volume of resumes and interviews (current) :notepad_spiral: 

- 

## Challenges (sourcing, process issues, etc.) that are slowing things down? :question: 

- 

----

Updates for the CI Partitioning Project are in a [separate issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/352)

----

cc: @gitlab-com/pipeline-execution-group This is the async update for ~"group::pipeline execution" this week.

/label ~"OpsSection::Weekly-Update" ~frontend ~backend ~"section::ops" ~"group::pipeline execution" ~"devops::verify" 
/confidential 
