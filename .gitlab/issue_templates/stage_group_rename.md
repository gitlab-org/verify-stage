### Introduction

When a group name is changed, we have several adminstrative tasks that must be comepleted. This issue will help track DRIs and tasks to be completed. 

### Team updates

- [ ] (individual) team.yml updates
- [ ] Handbook references - global rename of group
- [ ] Bamboo updates - `@Engineering Manager` to follow-up with `@glucchesi` 
- [ ] Triage reports and bot related automation - `@Product Manager`
- [ ] Sisense dashboard updates - `@Engineering Manager` to follow-up after bamboo updates are done 
- [ ] Comms:
   - [ ] Engineering Week in Review
   - [ ] #s_verify 
   - [ ] #development 
   - [ ] #eng_managers
- [ ] Google Groups - `@Engineering Manager`
- [ ] GitLab group `groupname` 
- [ ] Slack channels - `@Engineering Manager` 
- [ ] Rename: link to gitlab.com/gitlab-org group
   - [ ] Rename issues in gitlab.com/gitlab-org group
- [ ] Update (Group) retrospective project and templates
- [ ] Update async-retrospective yml name
- [ ] Geekbot 
- [ ] Bamboo updates for engineers (Request -> Job information change -> "Group Name" specialty)
  - [ ] `@Engineering Manager`

### Individual updates

Please update your Zoom, Slack, GitLab profiles, as necessary - e.g. rename `Verify:CI` to `Verify:Pipeline Execution`:
   - [ ] `@Engineer`
   - [ ] `@Engineer`
   - [ ] `@Engineer`


### Code updates in gitlab-org/gitlab

- [ ] Feature flags - `@Engineering Manager` 
- [ ] metrics - every instance of `groupname` 
  - [ ] usage ping docs: 
- [ ] `feature_category` references
