## Announcements

### Things to Celebrate

### Verify
(sources: [Verify Staff Meeting](https://docs.google.com/document/d/1R9Jj1-QntjQCQwnuLi9e4CpetKUsyo8AZ8kecDnHx7E/edit), Verify Slack channels)

### Department or Company-wide
(sources: [Engineering Week in Review](https://docs.google.com/document/d/1JBdCl3MAOSdlgq3kzzRmtzTsFWsTIQ9iQg0RHhMht6E/edit#), [Development Staff Meeting](https://docs.google.com/document/d/1d7CHiGOhdYdrKRyvT0Slz6fesHtH3dvPBu9Iqx3p41Y/edit), [Engineering Staff Meeting](https://docs.google.com/document/d/1uaBOKgqYM5-pDWBgfAuOqFYDAA56yVz76lgMvv8jUCQ/edit#))

### Reliability

~"devops::verify"

~"missed-SLO" ~infradev - [board view](https://gitlab.com/groups/gitlab-org/-/boards/4009838?not[label_name][]=type%3A%3Afeature&not[label_name][]=documentation&label_name[]=devops%3A%3Averify&label_name[]=infradev&label_name[]=missed-SLO)
- x past due
  - 
- https://app.periscopedata.com/app/gitlab/899982/InfraDev?widget=13349849&udv=1422719 from https://app.periscopedata.com/app/gitlab/899982/InfraDev

~"missed-SLO" ~security - [board view](https://gitlab.com/groups/gitlab-org/-/boards/4009838?not[label_name][]=type%3A%3Afeature&not[label_name][]=documentation&label_name[]=devops%3A%3Averify&label_name[]=security&label_name[]=missed-SLO)
- x past due
  - 
- https://app.periscopedata.com/app/gitlab/913607/Past-Due-Security-Issues-Development

### OKRs

- Trending xy% overall - https://app.ally.io/users/159023/objectives?tab=0&compare=month&time_period_id=155986
  - 
  
### Hiring 

**Currently hiring for x roles in Engineering!** :bulb: 
- Runner SaaS (3): BE Engineering Manager, BE Engineers (2) - Intermediate or Senior
- Pipeline Execution (4): BE Engineering Manager, BE Engineers (3) - Intermediate or Senior
- Pipeline Authoring (1): Senior BE Engineer
- Pipeline Insights (1): BE Engineer - Intermediate or Senior

/label ~"OpsSection::Weekly-Update"
