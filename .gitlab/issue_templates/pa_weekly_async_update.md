## Announcements :mega:

- 

## Milestone Tracking :calendar:
<!-- For example: list issues or epics being tracked, and % of completion. Add overall health status for the milestone (e.g. committed vs planned) -->
- [{insert milestone} Goals for milestone](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/{issue ID}#goals-for-the-milestone)
- [{insert milestone} Scope of work](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/{issue ID}#scope-of-work-for-engineering)
- [{insert milestone} Pipeline Authoring board](https://gitlab.com/gitlab-org/gitlab/-/boards/5726606?label_name[]=group%3A%3Apipeline%20authoring&milestone_title={insert milestone})

## Cross-functional prioritization ratios
| {insert previous milestone}  | ~"type::feature"  | ~"type::bug" | ~"type::maintenance" |
| --------| --------| --------| --------|
| EXPECTED (Targets) | % | % | % |
| ACTUALS (Closed issues) | % | % | % |

| {insert current milestone}  | ~"type::feature"  | ~"type::bug" | ~"type::maintenance" |
| --------| --------| --------| --------|
| EXPECTED (Targets) | % | % | % |

[Data Source](https://docs.google.com/spreadsheets/d/1h8wx4u5jGIzGyIoHKW7-dGvYp2ZUsMTt9V6zJBglYco/edit#gid=2125665651)

## Say/Do deliverable ratios for current milestone
| Say  | Do | Completion Rate (%) |
| --------| --------| --------|
|  |  | % |
  
[Data Source](https://docs.google.com/spreadsheets/d/1UAy0jXPPhnWM7tmy3WhWrjl4UHBiRpkisOESYeEY9BY/edit#gid=392598354)

### Completed this week :white_check_mark: 
<!-- Reference Tableau widgets -->
- Last 7 days of Issues closed.
- Last 7 days of MRs merged.

## Weekly Updates :pencil: 

### Incidents (if any)

None occurred this week.

### Error Budget :bar_chart: 

* Last week's 28d service [availability](https://dashboards.gitlab.net/d/stage-groups-detail-pipeline_authoring/stage-groups-pipeline-authoring-group-error-budget-detail?orgId=1) was {}%  
  * {} minutes remaining and {} minutes spent.
* The last 7d is [currently](https://dashboards.gitlab.net/d/stage-groups-detail-pipeline_authoring/stage-groups-pipeline-authoring-group-error-budget-detail?orgId=1&from=now-7d%2Fm&to=now%2Fm) at {}%

<!-- Can be a copy/paste from the Slack update -->

### Issues related to Reliability :chart_with_upwards_trend: 

<!-- You could add links to these queries so it's a matter of counting totals -->
- Infradev Past Due: [0](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20authoring&label_name%5B%5D=infradev&label_name%5B%5D=missed-SLO&first_page_size=20)
- Security Past Due: [0](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20authoring&label_name%5B%5D=security&label_name%5B%5D=missed-SLO&first_page_size=20) 
- Upcoming Security Past Due: [0](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=devops%3A%3Averify&label_name%5B%5D=security&label_name%5B%5D=type%3A%3Abug&label_name%5B%5D=group%3A%3Apipeline%20authoring&not%5Blabel_name%5D%5B%5D=type%3A%3Afeature&not%5Blabel_name%5D%5B%5D=documentation&not%5Blabel_name%5D%5B%5D=severity%3A%3A4&not%5Blabel_name%5D%5B%5D=test-plan&not%5Blabel_name%5D%5B%5D=missed-SLO&first_page_size=20)
- Corrective Action Past Due: [0](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Apipeline%20authoring&label_name%5B%5D=corrective%20action&label_name%5B%5D=missed-SLO&first_page_size=20)

### OKRs :bar_chart:

<!-- Link to OKRs, provide an updated score -->

[Current quarter OKR list](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=OKR&label_name%5B%5D=devops%3A%3Averify&label_name%5B%5D=group%3A%3Apipeline%20authoring&first_page_size=20)

{List out in numbered order OKRs}

{}% complete for ~"group::pipeline authoring" OKRs  

### Hiring :ballot_box_with_check: 

<!-- List any open reqs and each of their status -->

## Volume of resumes and interviews (current) :notepad_spiral: 

- 

## Challenges (sourcing, process issues, etc.) that are slowing things down? :question: 

- 

cc: @gitlab-com/pipeline-authoring-group This is the async update for ~"group::pipeline authoring" this week.

/label ~"OpsSection::Weekly-Update" ~frontend ~backend ~"section::ops" ~"group::pipeline authoring" ~"devops::verify" 
/assign marknuzzo
/confidential 
