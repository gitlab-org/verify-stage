<!--
Use issue title:
[YOUR NAME]: Documentation Contribution Tooling (FY##-Q#)
-->

### Overview :muscle: 

The technical writing team uses two lint tools to help improve the docs:

- [markdownlint](https://docs.gitlab.com/ee/development/documentation/testing.html#markdownlint):
  To catch non-standard formatting issues. Always fails the pipeline.
- [Vale](https://docs.gitlab.com/ee/development/documentation/testing.html#vale):
  To catch a wider range of issues, but also offer non-pipeline-breaking suggestions
  and warnings for people using Vale locally.

You can install these two tools locally to check for issues from the command line.
If you use Lefthook, it can run the linters automatically to [find docs issues pre-push](https://docs.gitlab.com/ee/development/documentation/testing.html#configure-pre-push-hooks).

If you [install the plugins into your editor](https://docs.gitlab.com/ee/development/documentation/testing.html#configure-editors),
you will get the kind of feedback you might get from a technical writer, but in your
editor before you actually send it for review!

Markdownlint and Vale errors already fail the pipeline, so you definitely want to
catch those before you push. Additionally, when you use the Vale plugin you'll start to
get non-breaking suggestions and warnings that will improve your docs and make docs
reviews even faster. Suggestions and warnings might look like:

- `Verify this use of the word "admin". Can it be updated to "administration", "administrator", "administer", or "Admin Area"?`
- `Use the US spelling "standardized" instead of the British "standardised".`
- `Avoid words like "currently" that promise future changes, because documentation is about the current state of the product.`
- `` Use "default branch" or `main` instead of `master`, when possible. ``
- See more rules at <https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc/.vale/gitlab)>

### Instructions :white_check_mark:

1. [ ] (Optional) Watch the walkthrough video from Marcel: https://youtu.be/Hgu8WsuLo7o
1. [ ] [Install the two linters](https://docs.gitlab.com/ee/development/documentation/testing.html#install-linters)
1. [ ] [Use them in your editor](https://docs.gitlab.com/ee/development/documentation/testing.html#configure-editors)
